'use strict';

const commando = require('discord.js-commando');
const r = require('request-promise');
const path = require('path');
const oneLine = require('common-tags').oneLine;
const configObject = require('./src/conf/config.js');

const config = new configObject();

const wcLog = require('./src/intervals/logs.js');

const client = new commando.Client({
	owner: config.owner_id,
	commandPrefix: config.prefix
});

client
	.on('error', console.error)
	.on('warn', console.warn)
	.on('debug', console.log)
	.on('ready', () => {
		console.log(`Client ready; logged in as ${client.user.username}#${client.user.discriminator} (${client.user.id})`);
	})
	.on('disconnect', () => { console.warn('Disconnected!'); })
	.on('reconnecting', () => { console.warn('Reconnecting...'); })
	.on('commandError', (cmd, err) => {
		if (err instanceof commando.FriendlyError) return;
		console.error(`Error in command ${cmd.groupID}:${cmd.memberName}`, err);
	})
	.on('commandBlocked', (msg, reason) => {
		console.log(oneLine`
			Command ${msg.command ? `${msg.command.groupID}:${msg.command.memberName}` : ''}
			blocked; ${reason}
		`);
	})
	.on('commandPrefixChange', (guild, prefix) => {
		console.log(oneLine`
			Prefix ${prefix === '' ? 'removed' : `changed to ${prefix || 'the default'}`}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('commandStatusChange', (guild, command, enabled) => {
		console.log(oneLine`
			Command ${command.groupID}:${command.memberName}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('groupStatusChange', (guild, group, enabled) => {
		console.log(oneLine`
			Group ${group.id}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('message', (msg) =>{
		// THE AYAYA TRAIN IS LEAVING THE STATION
		// FIXME: This will break in guilds the asides from <PE>. Hopefully they won't use our AYAYA
		if (msg.content.includes('<:AYAYA:586392461463781386>')) {
			msg.react(msg.guild.emojis.get('586392461463781386'))
		}
	});

client.registry
	.registerGroups([
		['text', 'Text'],
		['voice', 'Voice'],
		['admin', 'Admin']
	])
	.registerDefaults()
	.registerCommandsIn(path.join(__dirname, 'src/commands'));

client.login(config.discord_token);

//WCLog Initialization
if (config.wclOpts.apiKey !== undefined) {
	let wcLogPost = new wcLog(config, client);
} else {
	console.log('WCL_API_KEY not defined, skipping raid webhook...')
}
