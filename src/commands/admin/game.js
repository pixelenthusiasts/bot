'use strict';

const commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

module.exports = class GameCommand extends commando.Command {
  constructor(client) {
    super(client, {
      name: 'game',
      group: 'admin',
      memberName: 'game',
      description: 'Sets the game the bot is playing (admin only)',
      details: oneLine`
        This sets the game the bot is "playing" in its status!
			`,
      examples: ['game \'Orange Parse Simulator\''],

      userPermissions: ['ADMINISTRATOR'],

      args: [
        {
          key: 'game',
          label: 'game',
          prompt: 'What game is the bot playing?',
          type: 'string',
          infinite: false
        }
      ]
    });

  }

  async run(msg, args) {
    this.client.user.setGame(args.game);
  }
};