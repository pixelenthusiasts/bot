# PE Bot 2: Return of the Memes

Re-write of the original PixelEnthusiasts Bot using Discord.js-Commando

## Setup
### Prerequisites
Node 8.0.0+  
FFMPEG

### Required Environment Variables
`DISCORD_TOKEN`  
`OWNER_ID`  
`PREFIX`  

### Optional Environment Variables
`WCL_API_KEY`  
`RAID_WEBHOOK_ID`  
`RAID_WEBHOOK_TOKEN`  
`WCL_GUILD`  
`WCL_REALM` 

### Example `run.sh` file
```
#!/bin/bash

DISCORD_TOKEN="<YOUR BOT TOKEN>" \
WCL_API_KEY="<YOUR WARCRAFT LOGS API KEY>" \
RAID_GUILD_ID="<DISCORD GUILD ID W/ LOGS CHANNEL>" \
RAID_CHANNEL_ID="<DISCORD CHANNEL ID FOR LOGS>" \
WCL_GUILD="<WCL GUILD (URL-SAFE)>" \
WCL_REALM="<GUILD REALM (URL-SAFE)>" \
OWNER_ID="<DISCORD OWNER ID>" \
PREFIX="!" \
forever start index.js
```

### Guided installation

1. Setup a EC2/Compute instance with Ubuntu server (Preferable the latest LTS version).
1. Run `sudo apt-get update -y && apt-get install ffmpeg git` to install ffmpeg and git.
1. [Install nvm](https://github.com/nvm-sh/nvm#install--update-script).
1. Run `nvm install stable` to install the latest version of Node.js.
1. Run `npm install -g forever` to install the `forever` package. This will help restart the bot if it crashes for whatever reason.
1. `git clone` this repository where you'd like the bot to run.
1. `cd` into the directory and setup your own `run.sh` file like the one above (remember to `chmod +x`!)
1. You should now be setup to run the bot by executing `run.sh`! 

## Adding Sounds
To add a sound to the bot, it is a simple two-step process:  
1. Add your sound file (preferably .mp3, .m4a, or .ogg) to the `src/assets/sounds` folder.
1. In `src/assets/sounds.json`, add an entry in the following format: `"triggerKeyword": "soundFileName"`.

The `play` command automatically parses through the list in `sounds.json`, so no need to touch the code!